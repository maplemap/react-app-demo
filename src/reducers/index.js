import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import locale from './locale';
import errors from './errors';

export default combineReducers({
  routing: routerReducer,
  locale,
  errors
});
