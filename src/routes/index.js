import React from 'react';
import { Router, hashHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';

import App from '../containers/App';


function errorLoading(error) {
  console.error('Dynamic page loading failed', error);
}

function loadRoute(callback) {
  return module => callback(null, module.default);
}

const routes = {
  component: App,
  childRoutes: [
    {
      path: '/',
      getComponent(location, callback) {
        System.import('../components/Home')
          .then(loadRoute(callback))
          .catch(errorLoading);
      }
    },
    {
      path: 'login',
      getComponent(location, callback) {
        System.import('../containers/Login')
          .then(loadRoute(callback))
          .catch(errorLoading);
      }
    }
  ]
};

function getHistorySyncWithStore(store) {
  return syncHistoryWithStore(hashHistory, store);
}

const RootRoutes = ({ store }) => (
  <Router routes={routes} history={getHistorySyncWithStore(store)} />
);

export default RootRoutes;
