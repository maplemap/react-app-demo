import React from 'react';

const Login = props => (
  <div>
    Login Page <br /><br />
    <form>
      First name:<br />
      <input type="text" name="firstname" placeholder="firstname" />
      <br />
      Last name:<br />
      <input type="text" name="lastname" placeholder="lastname" />
      <br /><br />
      <input
        type="button"
        value="Submit"
        onClick={props.handlerFormClick}
      />
    </form>
  </div>
);

export default Login;
