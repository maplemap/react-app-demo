export default {
  locale: 'ru',
  api: {
    path: {
      translations: '/data/translations',
      projects: '/data/projects'
    }
  }
};
