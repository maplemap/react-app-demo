import React from 'react';
import { connect } from 'react-redux';

import Login from '../components/Login';

class LoginContainer extends React.Component {

  handlerFormClick() {
    console.log('form submit');
  }

  render() {
    console.log(this.props);

    return (
      <Login
        handlerFormClick={this.handlerFormClick}
      />
    );
  }
}

function mapStateToProps(state) {
  return state;
}

export default connect(mapStateToProps)(LoginContainer);
