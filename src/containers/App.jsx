import React from 'react';
import { Link } from 'react-router';


class App extends React.Component {
  render() {
    return (
      <div className="App">
        <menu>
          <li><Link to="/">Home</Link></li>
          <li><Link to="login">Login</Link></li>
        </menu>
        <div className="content">
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default App;
